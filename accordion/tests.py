from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from django.urls import resolve
from .views import index
import time

# Create your tests here.
class story8UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

    def test_using_certain_template(self):
        response = Client().get('/accordion/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_using_certain_function(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, index)

class story8FunctionalTest(LiveServerTestCase):
    def initialize_selenium(self):
        opt = webdriver.ChromeOptions()
        opt.add_argument('--headless')
        opt.add_argument('--no-sandbox')
        opt.add_argument('--dns-prefetch-disable')
        opt.add_argument('disable-gpu')
        return webdriver.Chrome('./chromedriver', chrome_options=opt)

    def test_swap_position(self):
        driver = self.initialize_selenium()
        driver.get(self.live_server_url+'/accordion')
        panel_group = driver.find_element_by_class_name('panel-group')
        panels = panel_group.find_elements_by_class_name('panel')

        panel_1_text = panels[0].find_element_by_class_name('panel-heading').find_element_by_class_name('panel-title').find_element_by_tag_name('a').text
        panel_2_text = panels[1].find_element_by_class_name('panel-heading').find_element_by_class_name('panel-title').find_element_by_tag_name('a').text

        # Put panel #2 to #1
        panels[1].find_element_by_class_name('panel-heading').find_element_by_class_name('panel-title').find_element_by_class_name('button-up').click()

        panel_group_after = driver.find_element_by_class_name('panel-group')
        panels_after = panel_group_after.find_elements_by_class_name('panel')

        panel_1_text_after = panels_after[0].find_element_by_class_name('panel-heading').find_element_by_class_name('panel-title').find_element_by_tag_name('a').text
        panel_2_text_after = panels_after[1].find_element_by_class_name('panel-heading').find_element_by_class_name('panel-title').find_element_by_tag_name('a').text

        self.assertEqual(panel_1_text_after, panel_2_text)
        self.assertEqual(panel_2_text_after, panel_1_text)

    def test_dark_mode_challenge(self):
        driver = self.initialize_selenium()
        driver.get(self.live_server_url+'/accordion')
        dark_mode_switcher = driver.find_element_by_id("mode-switch")
        dark_mode_switcher.click()

        body = driver.find_element_by_tag_name('body')
        from selenium.webdriver.support.color import Color

        self.assertEqual(Color.from_string(body.value_of_css_property("background-color")).hex, '#000000')

