from django.urls import path
from .views import index

urlpatterns = [
    path('accordion/', index, name='index'),
]