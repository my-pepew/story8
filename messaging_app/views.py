from django.shortcuts import render, redirect
from .models import Message

# Create your views here.
def index(request):
    ctx = {}
    messages = Message.objects.all()
    ctx['messages'] = messages
    return render(request, 'add_comment.html', ctx)

def confirm(request):
    ctx = {}
    if request.method == 'POST':
        ctx['name'] = request.POST.get('name')
        ctx['message'] = request.POST.get('message')
        return render(request, 'confirmation.html', ctx)

    return redirect('index')

def save(request):
    ctx = {}
    if request.method == 'POST':
        name = request.POST.get('name')
        message = request.POST.get('message')
        msg = Message(name=name, message=message)
        msg.save()
        return redirect('index')

    return redirect('index')

def update(request):
    ctx = {}
    if request.method == 'POST':
        id = request.POST.get('id')
        color = request.POST.get('color')
        try:
            msg = Message.objects.get(pk=id)
        except:
            return redirect('index')

        msg.color = color
        msg.save()

    return redirect('index')