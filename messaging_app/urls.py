from django.urls import path
from .views import index, confirm, save, update

urlpatterns = [
    path('comment/', index, name='index'),
    path('confirm/', confirm),
    path('save/', save),
    path('update/', update),
]