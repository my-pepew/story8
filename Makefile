test:
	coverage run --include='accordion/*' manage.py test
	coverage report -m

run:
	python3 manage.py runserver

migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate